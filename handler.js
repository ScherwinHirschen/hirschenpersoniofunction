const fetch = require("node-fetch");
const FormData = require('form-data');

module.exports.handler = async (req, context, callback) => {

    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'json/application'
        },
        body: 'Personio Pipe'
    };
    const { PERSONIO_KEY, COMPANY_ID } = process.env;
    const { files, body } = req;

    const form = new FormData;
    form.append("access_token",PERSONIO_KEY);
    form.append("company_id", COMPANY_ID);

    try{
        if(Array.isArray(files)) {
            files.forEach(file => {
                console.log(file);
                form.append("documents[]", file.buffer, {filename: file.originalname});
            });
        }

        Object.keys(body).forEach(key => {
            if(key === "job_position_id") form.append(key,body[key]);
            else if(key !== "recruiting_channel_id") form.append(key,body[key]);
        });

        const fetchResult = await fetch("https://api.personio.de/recruiting/applicant",{
            method: "POST",
            body: form,
        });
        const {status} = fetchResult;
        response.statusCode = status;

        if(status === 200){
            response.body = await fetchResult.json();
        } else if(status===413){
            response.body = {
                error: await fetchResult.text()
            };
        } else{
            response.body = JSON.parse(await fetchResult.text());
        }
        callback(null, response);
    }catch (e) {
        console.log(e);
    }
};
