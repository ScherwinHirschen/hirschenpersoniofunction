const app = require('express')()
const handler = require('./handler').handler
const routes = require('./function').routes
const serverless = require('serverless-http')
const multer = require('multer');
const upload = multer();
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

const {0: route} = routes;
app.post('/' + route.path, upload.array("documents[]",10), (req, res) => {
    handler(req, null, (err, response) => {
        res.set(response.headers);
        res.status(response.statusCode);
        res.send(response.body)
    })
});
module.exports.handler = serverless(app);
