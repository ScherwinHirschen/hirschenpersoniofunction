const app = require('express')()
const handler = require('./handler').handler
const routes = require('./function').routes
const multer = require('multer');
const upload = multer();
//app.use(bodyParser.json())
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
const PORT = process.env.PORT || 3300
routes.forEach(route => {

    if (route.method === 'get') {
        app.get('/' + route.path, (req, res) => {
            const event = {
                body: JSON.stringify(req.body)
            };
            handler(event, null, (err, response) => {
                res.send(response.body)
            })
        })
    }

    if (route.method === 'post') {

        app.post('/' + route.path, upload.array("documents[]",10), (req, res) => {
            handler(req, null, (err, response) => {
                res.set(response.headers);
                res.status(response.statusCode);
                res.send(response.body)
            })
        })

    }
    if (route.method === 'put') {
        app.put('/' + route.path, (req, res) => {
            const event = {
                body: JSON.stringify(req.body)
            }
            handler(event, null, (err, response) => {
                res.send(response.body)
            })
        })
    }

    if (route.method === 'delete') {
        app.del('/' + route.path, (req, res) => {
            const event = {
                body: JSON.stringify(req.body)
            }
            handler(event, null, (err, response) => {
                res.send(response.body)
            })
        })
    }
})
app.listen(PORT)
console.log(`Listening on port ${PORT}`)
